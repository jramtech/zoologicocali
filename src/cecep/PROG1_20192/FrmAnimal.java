package cecep.PROG1_20192;
import java.awt.Color; //Tiene como funcion de cambiar la apariencia del objeto
import java.awt.Image; //Contiene clases e interfaces para manipular imágenes.
import java.util.ArrayList; // Esta librería me permite el uso de arrayList o listas dinámicas 
import java.util.HashMap;  //es una clase de colección basada en el mapa que se utiliza para almacenar pares de clave y valor
import java.util.Map;      //un objeto que asigna claves a valores. Un mapa no puede contener claves duplicadas: cada clave puede mapearse a un valor como máximo. Modela la abstracción de la función matemática
//indica que todos los mensajes deben estar registrados.
import java.util.logging.Level; //Proporciona las clases e interfaces de las instalaciones de registro central de la plataforma Java 
import java.util.logging.Logger; //Es la clase utilizada para registrar mensajes de aplicaciones en la API de registro de Java.
import javax.swing.DefaultComboBoxModel; //Construye un objeto vacío
import javax.swing.ImageIcon; //Una implementación de la interfaz de iconos que pinta iconos de imágenes. Las imágenes que se crean a partir de una URL, nombre de archivo o matriz de bytes.
import javax.swing.JOptionPane; //Esta clase hace realmente fácil el mostrar ventanas standards para mostrar y pedir información a los usuarios.
//Estas librerias tepermiten utlizar el JASREPOR
import net.sf.jasperreports.engine.JREmptyDataSource; 
import net.sf.jasperreports.engine.JRException; //Saca los errores
import net.sf.jasperreports.engine.JasperCompileManager; //compilar el archivo
import net.sf.jasperreports.engine.JasperFillManager;//Permite manipular la informacción dentro del archivo
import net.sf.jasperreports.engine.JasperPrint;// imprimir el archivo
import net.sf.jasperreports.engine.JasperReport;//
import net.sf.jasperreports.view.JasperViewer;//Me permite visualizar el archivo

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jose R
 */
public class FrmAnimal extends javax.swing.JFrame {
    //Datos globales de la clase
    Boolean DeMapas = false;//Bool usado para determinar a donde lleva el boton retornar
    ArrayList<String> MantRealizado = new ArrayList<>();
    public static InfoSesion sesion;//Uso el nodo InfoSesion creado en FrmLogin
                                       //para obtener informacion sobre el usuario
    
    public FrmAnimal(InfoSesion sesion) {
        initComponents();
        //Declaracion de las variables 
        this.sesion = sesion;
        //Se crea una caspeta y se llama la imagen
        ImageIcon Fondo = new ImageIcon("src/Imagenes/FondoFrmAnimal.png");
        //Esta linea de codigo sirve para asignarles las propiedades al jlabel, dandole el ancho y el alto.
        ImageIcon ScaledFondo = new ImageIcon(Fondo.getImage().getScaledInstance(LB_Fondo.getWidth(), LB_Fondo.getHeight(), Image.SCALE_DEFAULT));
        LB_Fondo.setIcon(ScaledFondo);//Imagen de fondo
        this.setLocationRelativeTo(null);//Ventana en el medio de la pantalla
        CB_Especie.setModel(new DefaultComboBoxModel<>(makeCmbList("especie")));//llena el combobox especie
                                                                            //con lo que retorna makecmblist
        CB_Especie.setForeground(Color.LIGHT_GRAY);//color gris para los placeholders
        CB_Raza.setForeground(Color.LIGHT_GRAY);//color gris para los placeholders
        CB_Raza.addItem("Seleccione Especie");//placeholder
        CB_Animal.setForeground(Color.LIGHT_GRAY);//color gris para los placeholders
        CB_Animal.addItem("Seleccione Especie");//placeholder

        if (sesion == null) {
            sesion = new InfoSesion(new String[]{"01", "jramirez"});//Para pruebas de este archivo 
        }                                  //sin correr todo el programa.
                                           //Usa una cuenta de cuidador "02" o de admin "01"
        if (sesion.getCargo().equalsIgnoreCase("Cuidador")) {//Si el usuario es un cuidador se deshabilita 
                                                            //La ventanilla de diagnostico... Si no es cuidador
                                                            //solo puede ser veterinario o administrador
                                                            //y los dos tienen acceso a la ventanilla.
            Mantenimiento.setEnabledAt(1, false);
        }
        HabilitarPaneles(false);//Deshabilitar paneles ya que no hay animal seleccionado
    }

    private void LoadImg(String animal){//metodo q retorna una imagen guardada en memoria y recibe el nombre como parametro
        String ruta = "/Imagenes/Animales/"+animal+".JPG";//Crea una variable tipo String que en su interior tiene una URL, la cual depende del tipo de animal
        try { //Se usa el try catch para imprimir algun tipo de error que haya en el codigo
            String fichero = (getClass().getResource(ruta)).toString();
            if (fichero.contains("file:")) fichero = fichero.replaceAll("file:", "");//remover "file:" de la ruta del archivo
            ImageIcon Iconanimal = new ImageIcon(fichero);
            Iconanimal = new ImageIcon(Iconanimal.getImage().getScaledInstance(LB_ImgAnimal.getWidth(), LB_ImgAnimal.getHeight(), Image.SCALE_DEFAULT));//Define las dimensiones del ImageIcon
            LB_ImgAnimal.setIcon(Iconanimal);  //Inserta en el LB_ImgAnimal la imagen previamente definida
        } catch (Exception e) {  //Captura el tipo de error
            System.out.println("Imagen para el animal: '"+animal+"' no encontrada."); //Imprime un mensaje del error
            LB_ImgAnimal.setIcon(null); //Si el try catch da error entonces hace que el LB_ImgAnimal sea nulo o que este vacio
        }
        
    }
    private void PrintReport(String diagnostico) throws JRException{//Ignora errores de jasperreports
        //Compila el archivo jrxml y lo guarda en memoria
        JasperReport jr = JasperCompileManager.compileReport((getClass().getResource("/reportes/report.jrxml").toString()).replaceAll("file:", ""));
        //Creo un mapa parametros para pasarle al reporte
        Map parameters = new HashMap<String, Object>();
        //Agrego paramateros
        parameters.put("Name", sesion.getNombre_completo());
        parameters.put("Cargo", sesion.getEspecializacion());
        parameters.put("Diagnostico", diagnostico);
        parameters.put("Animal", LB_NombreFill.getText());
        parameters.put("Edad", LB_EdadFill.getText());
        parameters.put("Sexo", LB_SexoFill.getText());
        parameters.put("Peso", LB_PesoFill.getText());
        parameters.put("Estatura", LB_EstaturaFill.getText());
        //Lleno el reporte con los parametros y uso un datasource vacio
        JasperPrint jp = JasperFillManager.fillReport(jr, parameters, new JREmptyDataSource());
        //Muestro la ventana
        JasperViewer.viewReport(jp, false);
    }
    private void AgregarMant(String mantenimiento){ //Es un metodo privado llamado AgregarMant con un parametro tipo String llamado mantenimiento
        boolean continuar = true; //Dato de tipo booleano que es verdadero
        for (String string : MantRealizado) { //Variable tipo cadena que recorre el vector llamado MantRealizado y selecciona las variables que contienen string y hace lo siguiente
            if (string.equalsIgnoreCase(mantenimiento))continuar=false; //Si la variable llamada string ignora cualquier tipo cualquier Mayusculas o Minusculas dentro de la condicional Matenimiento es falso
        }
        if(continuar)MantRealizado.add(mantenimiento);
    }
    private void HabilitarPaneles(Boolean b){
        SPanel_Mantenimiento.setVisible(b); //Muestra el Panel de mantenimiento
        Panel_Diagnostico.setVisible(b);   //Muestra el panel del diagnostico
        TA_Diagnostico1.setText("");        //vacia TA_Diagnostico1
        TA_Observaciones.setText("");      //vacia TA_Observaciones
        CkB_Alimentacion.setSelected(false);  //El CheckBox de la alimentacion queda desmarcado
        CkB_Limpieza.setSelected(false);      //El CheckBox Limpieza queda desmarcado
        CkB_Vitaminas.setSelected(false);    //El CheckBox Vitaminas queda desmarcado
        CmB_Estado.setSelectedIndex(0);     //El CheckBox Estado queda desmarcado
    }
    private void PanelMantenimiento(String cod_animal) {//Metodo que maneja el panel de mantenimientos
        String cod_habitat = "", cod_alimentacion = "";
        String[] habitat = null, alimentacion = null;
        for (String[] animal : Matrices.animales) {//recorre la matriz animales
            if (animal[0].contains(cod_animal)) {//Si encuentra un animal con el cod
                cod_habitat = animal[0].substring(6, 8);//Guarda el cod referencia del habitat
                cod_alimentacion = animal[0].substring(8, 10);//Guarda el cod referencia de la alimentacion
            }
        }
        for (String[] element : Matrices.habitat) {//Recorre la matriz habitat
            if (element[0].equals(cod_habitat)) {//Busca el codigo que consegui arriba
                habitat = element;//Guarda el resto de los datos en el vector
            }
        }
        for (String[] element : Matrices.tipo_alimentacion) {//Recorre la matriz alimentacion
            if (element[0].equals(cod_alimentacion)) {//Busca el codigo que consegui arriba
                alimentacion = element;//Guarda el resto de los datos en el vector
            }
        }
        CmB_Estado.setModel(new DefaultComboBoxModel<>(makeCmbList("estado")));//llena el combobox especie
        CkB_Alimentacion.setText("Alimentacion de '" + alimentacion[1] + "' " + alimentacion[2] + " veces al dia.");
        CkB_Limpieza.setText("Limpieza del habitat '" + habitat[1] + "' cada " + habitat[2] + " dias.");
    }
    public void FiltroAnimal(String Animal) {//es publico ya que frmMapa lo va a usar
        DeMapas = true;
        String codigo = (ExpandirCodRef(Animal, "animal")[0]);//Expando el codigo del animal para incluir raza y especie
        String codigo_especie = codigo.substring(0, 2);
        String codigo_raza = codigo.substring(2, 4);
        String codigo_animal = codigo.substring(4, 6);
        for (int i = 0; i < CB_Especie.getItemCount(); i++) {//recorre el combobox y selecciona la especie correcta
            if (codigo_especie.equals(CB_Especie.getItemAt(i).substring(0, 2))) {
                CB_Especie.setSelectedIndex(i);
            }
        }
        for (int i = 0; i < CB_Raza.getItemCount(); i++) {//recorre el combobox y selecciona la raza correcta
            if (codigo_raza.equals(CB_Raza.getItemAt(i).substring(0, 2))) {
                CB_Raza.setSelectedIndex(i);
            }
        }
        for (int i = 0; i < CB_Animal.getItemCount(); i++) {//recorre el combobox y selecciona el animal
            if (codigo_animal.equals(CB_Animal.getItemAt(i).substring(0, 2))) {
                CB_Animal.setSelectedIndex(i);
            }
        }
    }
    private String[] makeCmbList(String seleccion) {//Este metodo crea vectores de cadenas para llenar los combobox
        ArrayList<String> temp = new ArrayList<>();//Declaro el vector
        if (seleccion.equals("especie")) {//Define si el vector es para el combobox especie
            temp.add("Seleccione Especie");//agrega placeholder
            for (String[] element : Matrices.especie) {//recorre la matriz especie
                temp.add(element[0] + "." + element[1]);//Agrega a la lista temp "cod+nombre"
            }
        } else if (seleccion.equals("estado")) {
            temp.add("Seleccione Estado");//agrega placeholder
            for (String[] element : Matrices.estado) {//recorre la matriz especie
                temp.add(element[0] + "." + element[1]);//Agrega a la lista temp "cod+nombre"
            }
        } else {//Si el parametro no es 'especie' es una seleccion de un combobox
            String cod = (seleccion.split("\\.", 2))[0];//Separo el codigo del nombre de la seleccion
            if (cod.length() == 2) {//Si el codigo es de 2 numeros, es una seleccion del combobox especie
                temp.add("Seleccione Raza");//Agrega placeholder
                for (String[] element : Matrices.razas) {//Recorro la matriz de razas
                    if ((element[0].substring(0, 2)).equals(cod)) {//Si encuentra una raza perteneciente a la especie
                        temp.add((element[0].substring(2, 4)) + "." + element[1]);//Agrega a la lista temp "cod+nombre"
                    }
                }
            } else {//Si el codigo no es de 2 numeros solo puede provenir de una seleccion del combobox raza
                    //Ya que el codigo de los elementos de raza tienen 4 numeros (Despues de expandirlo claro..)
                temp.add("Seleccione Animal");//Agrega placeholder
                for (String[] element : Matrices.animales) {//Recorre matriz animales
                    if ((element[0].substring(0, 4)).equals(cod)) {//Si encuentra un animal perteneciente a la raza
                        temp.add((element[0].substring(4, 6)) + "." + element[1]);//Agrega a la lista temp "cod+nombre"
                    }
                }
            }
        }
        return temp.toArray(new String[0]);//Convierte la lista a un vector de cadenas antes de retornarlo
    };
    private void InfoAnimal(String cod_animal) {//Este metodo llena la informacion de los animales
        for (String[] animal : Matrices.animales) {//recorre la matriz animales
            if (animal[0].contains(cod_animal)) {//si el codigo del animal siendo iterado contiene
                                                //el codigo pasado como argumento (el cual tiene 6 numeros
                                                //2 de especie 2 de raza 2 de animal.
                                                //uso contains ya que los codigos no van a ser iguales porque
                                                //los codigos en la matriz incluyen otras cosas como alimentacio y habitat
                LB_NombreFill.setText(animal[1] + " (" + animal[2] + ")");
                LB_VenenoFill.setText(animal[7]);
                LB_EdadFill.setText(animal[5]);
                LB_SexoFill.setText(animal[6]);
                LB_PesoFill.setText(animal[3]);
                LB_EstaturaFill.setText(animal[4]);
                LB_ExtincionFill.setText(animal[8]);
                LoadImg(animal[1]);
                HabilitarPaneles(true);
                PanelMantenimiento(cod_animal);
            }
        }
    }
    private String[] ExpandirCodRef(String Seleccion, String CBCALL) {//Este metodo expande el codigo de una seleccion
                                                                    //Si tiene referencias padres (como raza y animales)
        String SelVector[] = Seleccion.split("\\.", 2);//Separa la seleccion en un vector conteniendo
                                                    //en [0] el codigo y en [1] el nombre
        String[][] matriz;//Declara una matriz en donde guardar la matriz de la seleccion
                        //Esto es para no repetir codigo
        int delim;//Un entero el cual define la cantidad de numeros por expandir al codigo de la seleccion
        if (CBCALL.equalsIgnoreCase("Raza")) {//Si este metodo fue llamado por el evento del combobox raza
                                            //Esta seleccion continua ya que el evento pasa un parametro
                                            //a este metodo con su nombre y lo referencio con "CBCALL"
            matriz = Matrices.razas;//Guarda la matriz razas en matriz..
            delim = 2;//Las selecciones de raza necesitan expandir su codigo por 2 numeros para incluir
                    //2 de la especie
        } else {//Si no es raza solo puede ser animales
            matriz = Matrices.animales;//Guarda la matriz animales en matriz..
            delim = 4;//Las selecciones de animal necesitan expandir su codigo por 4 numeros para incluir
                    //2 de la raza y 2 de la especie
        }
        for (String[] element : matriz) {//Recorre la matriz seleccionada raza/animales
            String cod_padre = element[0].substring(0, delim);//Guarda el codigo padre (los numeros por agregar)
            if (SelVector[1].equalsIgnoreCase(element[1])) {//Si el nombre de la seleccion es igual al elemento
                                                            //Siendo recorrido en la matriz
                SelVector[0] = cod_padre + SelVector[0];//Agrega el codigo padre al codigo de la seleccion
                break;//Se sale del ciclo
            }
        }
        return SelVector;//Retorna el vector con [0] el codigo completo y [1] el nombre de la seleccion
                        //Lo retorno como una matriz y no una cadena por si necesito usar solo el codigo
                        //Me ahorro tener que separarlos otra vez
    };

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LB_ImgAnimal = new javax.swing.JLabel();
        LB_NombreFill = new javax.swing.JLabel();
        LB_Edad = new javax.swing.JLabel();
        LB_EdadFill = new javax.swing.JLabel();
        LB_Sexo = new javax.swing.JLabel();
        LB_SexoFill = new javax.swing.JLabel();
        LB_Peso = new javax.swing.JLabel();
        LB_PesoFill = new javax.swing.JLabel();
        LB_Estatura = new javax.swing.JLabel();
        LB_EstaturaFill = new javax.swing.JLabel();
        LB_Veneno = new javax.swing.JLabel();
        LB_VenenoFill = new javax.swing.JLabel();
        LB_Extincion = new javax.swing.JLabel();
        LB_ExtincionFill = new javax.swing.JLabel();
        LB_Especie = new javax.swing.JLabel();
        CB_Especie = new javax.swing.JComboBox<>();
        LB_Raza = new javax.swing.JLabel();
        CB_Raza = new javax.swing.JComboBox<>();
        LB_Animal = new javax.swing.JLabel();
        CB_Animal = new javax.swing.JComboBox<>();
        BT_regreso = new javax.swing.JButton();
        BT_cerrar = new javax.swing.JButton();
        Mantenimiento = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        SPanel_Mantenimiento = new javax.swing.JScrollPane();
        Panel_Mantenimiento = new javax.swing.JPanel();
        LB_Estado = new javax.swing.JLabel();
        CmB_Estado = new javax.swing.JComboBox<>();
        CkB_Limpieza = new javax.swing.JCheckBox();
        CkB_Alimentacion = new javax.swing.JCheckBox();
        CkB_Vitaminas = new javax.swing.JCheckBox();
        LB_Observaciones = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TA_Observaciones = new javax.swing.JTextArea();
        BT_GuardarObservaciones = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        TPanel_Diagnostico = new javax.swing.JPanel();
        Panel_Diagnostico = new javax.swing.JPanel();
        BT_ImprimirDiagnostico = new javax.swing.JButton();
        Spane_TA_Diag = new javax.swing.JScrollPane();
        TA_Diagnostico1 = new javax.swing.JTextArea();
        LB_Fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(680, 430));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LB_ImgAnimal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(LB_ImgAnimal, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 160, 110));

        LB_NombreFill.setFont(new java.awt.Font("Arial", 3, 12)); // NOI18N
        LB_NombreFill.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(LB_NombreFill, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 30, 260, 15));

        LB_Edad.setFont(new java.awt.Font("Arial", 3, 12)); // NOI18N
        LB_Edad.setText("Edad:");
        getContentPane().add(LB_Edad, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 50, -1, -1));

        LB_EdadFill.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(LB_EdadFill, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 50, 100, 15));

        LB_Sexo.setFont(new java.awt.Font("Arial", 3, 12)); // NOI18N
        LB_Sexo.setText("Sexo:");
        getContentPane().add(LB_Sexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 70, -1, -1));

        LB_SexoFill.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(LB_SexoFill, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, 30, 15));

        LB_Peso.setFont(new java.awt.Font("Arial", 3, 12)); // NOI18N
        LB_Peso.setText("Peso:");
        getContentPane().add(LB_Peso, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 70, -1, -1));

        LB_PesoFill.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(LB_PesoFill, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 70, 100, 15));

        LB_Estatura.setFont(new java.awt.Font("Arial", 3, 12)); // NOI18N
        LB_Estatura.setText("Estatura:");
        getContentPane().add(LB_Estatura, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 90, -1, -1));

        LB_EstaturaFill.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(LB_EstaturaFill, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 90, 99, 15));

        LB_Veneno.setFont(new java.awt.Font("Arial", 3, 12)); // NOI18N
        LB_Veneno.setText("Veneno:");
        getContentPane().add(LB_Veneno, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 50, -1, -1));

        LB_VenenoFill.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(LB_VenenoFill, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 50, 20, 15));

        LB_Extincion.setFont(new java.awt.Font("Arial", 3, 12)); // NOI18N
        LB_Extincion.setText("Peligro Extincion:");
        getContentPane().add(LB_Extincion, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 110, -1, -1));

        LB_ExtincionFill.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(LB_ExtincionFill, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 110, 110, 15));

        LB_Especie.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LB_Especie.setForeground(new java.awt.Color(255, 255, 255));
        LB_Especie.setText("Especies");
        getContentPane().add(LB_Especie, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 80, -1));

        CB_Especie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CB_EspecieActionPerformed(evt);
            }
        });
        getContentPane().add(CB_Especie, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 160, -1));

        LB_Raza.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LB_Raza.setForeground(new java.awt.Color(255, 255, 255));
        LB_Raza.setText("Razas");
        getContentPane().add(LB_Raza, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 50, -1));

        CB_Raza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CB_RazaActionPerformed(evt);
            }
        });
        getContentPane().add(CB_Raza, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 160, -1));

        LB_Animal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LB_Animal.setForeground(new java.awt.Color(255, 255, 255));
        LB_Animal.setText("Animal");
        getContentPane().add(LB_Animal, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, 70, -1));

        CB_Animal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CB_AnimalActionPerformed(evt);
            }
        });
        getContentPane().add(CB_Animal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 160, -1));

        BT_regreso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/RegresarIcono.png"))); // NOI18N
        BT_regreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BT_regresoActionPerformed(evt);
            }
        });
        getContentPane().add(BT_regreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 400, 30, 30));

        BT_cerrar.setBackground(new java.awt.Color(255, 255, 255));
        BT_cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/CerrarIcono.png"))); // NOI18N
        BT_cerrar.setFocusable(false);
        BT_cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BT_cerrarActionPerformed(evt);
            }
        });
        getContentPane().add(BT_cerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 0, 20, 20));

        Mantenimiento.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));

        Panel_Mantenimiento.setMaximumSize(new java.awt.Dimension(424, 395));
        Panel_Mantenimiento.setMinimumSize(new java.awt.Dimension(424, 395));
        Panel_Mantenimiento.setPreferredSize(new java.awt.Dimension(424, 395));

        LB_Estado.setText("Estado de salud");

        CmB_Estado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        CkB_Limpieza.setToolTipText("Las instalaciones(Jaulas) de los animales se hacen en base a un habitat.");
        CkB_Limpieza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CkB_LimpiezaActionPerformed(evt);
            }
        });

        CkB_Alimentacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CkB_AlimentacionActionPerformed(evt);
            }
        });

        CkB_Vitaminas.setText("Suplementos Vitaminicos");
        CkB_Vitaminas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CkB_VitaminasActionPerformed(evt);
            }
        });

        LB_Observaciones.setText("Otras observaciones:");

        TA_Observaciones.setColumns(20);
        TA_Observaciones.setRows(5);
        jScrollPane1.setViewportView(TA_Observaciones);

        BT_GuardarObservaciones.setText("Guardar");
        BT_GuardarObservaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BT_GuardarObservacionesActionPerformed(evt);
            }
        });

        jLabel1.setText("Mantenimientos realizados:");
        jLabel1.setToolTipText("Marcar todos los que apliquen");

        javax.swing.GroupLayout Panel_MantenimientoLayout = new javax.swing.GroupLayout(Panel_Mantenimiento);
        Panel_Mantenimiento.setLayout(Panel_MantenimientoLayout);
        Panel_MantenimientoLayout.setHorizontalGroup(
            Panel_MantenimientoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Panel_MantenimientoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BT_GuardarObservaciones)
                .addGap(16, 16, 16))
            .addGroup(Panel_MantenimientoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Panel_MantenimientoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CkB_Limpieza, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(CkB_Alimentacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(CkB_Vitaminas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(Panel_MantenimientoLayout.createSequentialGroup()
                        .addGroup(Panel_MantenimientoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LB_Observaciones)
                            .addComponent(jLabel1)
                            .addGroup(Panel_MantenimientoLayout.createSequentialGroup()
                                .addComponent(LB_Estado)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(CmB_Estado, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 12, Short.MAX_VALUE)))
                .addContainerGap())
        );
        Panel_MantenimientoLayout.setVerticalGroup(
            Panel_MantenimientoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panel_MantenimientoLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(Panel_MantenimientoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LB_Estado, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmB_Estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CkB_Limpieza)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CkB_Alimentacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CkB_Vitaminas)
                .addGap(18, 18, 18)
                .addComponent(LB_Observaciones)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BT_GuardarObservaciones))
        );

        SPanel_Mantenimiento.setViewportView(Panel_Mantenimiento);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 446, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(SPanel_Mantenimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 446, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 213, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(SPanel_Mantenimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        Mantenimiento.addTab("Mantenimiento", jPanel1);

        TPanel_Diagnostico.setMaximumSize(new java.awt.Dimension(446, 213));
        TPanel_Diagnostico.setMinimumSize(new java.awt.Dimension(446, 213));

        Panel_Diagnostico.setMaximumSize(new java.awt.Dimension(446, 213));
        Panel_Diagnostico.setMinimumSize(new java.awt.Dimension(446, 213));

        BT_ImprimirDiagnostico.setText("Imprimir");
        BT_ImprimirDiagnostico.setToolTipText("");
        BT_ImprimirDiagnostico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BT_ImprimirDiagnosticoActionPerformed(evt);
            }
        });

        Spane_TA_Diag.setMaximumSize(new java.awt.Dimension(446, 213));
        Spane_TA_Diag.setMinimumSize(new java.awt.Dimension(446, 213));
        Spane_TA_Diag.setPreferredSize(new java.awt.Dimension(446, 213));

        TA_Diagnostico1.setColumns(20);
        TA_Diagnostico1.setRows(5);
        Spane_TA_Diag.setViewportView(TA_Diagnostico1);

        javax.swing.GroupLayout Panel_DiagnosticoLayout = new javax.swing.GroupLayout(Panel_Diagnostico);
        Panel_Diagnostico.setLayout(Panel_DiagnosticoLayout);
        Panel_DiagnosticoLayout.setHorizontalGroup(
            Panel_DiagnosticoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Panel_DiagnosticoLayout.createSequentialGroup()
                .addContainerGap(373, Short.MAX_VALUE)
                .addComponent(BT_ImprimirDiagnostico)
                .addContainerGap())
            .addGroup(Panel_DiagnosticoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(Panel_DiagnosticoLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(Spane_TA_Diag, javax.swing.GroupLayout.PREFERRED_SIZE, 434, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        Panel_DiagnosticoLayout.setVerticalGroup(
            Panel_DiagnosticoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Panel_DiagnosticoLayout.createSequentialGroup()
                .addContainerGap(184, Short.MAX_VALUE)
                .addComponent(BT_ImprimirDiagnostico)
                .addContainerGap())
            .addGroup(Panel_DiagnosticoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(Panel_DiagnosticoLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(Spane_TA_Diag, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(37, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout TPanel_DiagnosticoLayout = new javax.swing.GroupLayout(TPanel_Diagnostico);
        TPanel_Diagnostico.setLayout(TPanel_DiagnosticoLayout);
        TPanel_DiagnosticoLayout.setHorizontalGroup(
            TPanel_DiagnosticoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TPanel_DiagnosticoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(Panel_Diagnostico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        TPanel_DiagnosticoLayout.setVerticalGroup(
            TPanel_DiagnosticoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TPanel_DiagnosticoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(Panel_Diagnostico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        Mantenimiento.addTab("Diagnosticos", TPanel_Diagnostico);

        getContentPane().add(Mantenimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(221, 156, 450, 240));
        getContentPane().add(LB_Fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 680, 430));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CB_AnimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CB_AnimalActionPerformed
        String item = (String) CB_Animal.getSelectedItem();//Guardo el elemento seleccionado en el combobox familias en una variable
        if (item != null && !(item.contains("Seleccione"))) {//Si la seleccion es un placeholder, ignorarla
            if (CB_Animal.getItemAt(0).contains("Seleccione")) {
                CB_Animal.removeItemAt(0);//Remueve el placeholder
            }
            CB_Animal.setForeground(Color.BLACK);//Cambio el color de texto a negro
            InfoAnimal((ExpandirCodRef(item, "animal"))[0]);//Envio al metodo infoanimal el codigo completo de la seleccion
        }
    }//GEN-LAST:event_CB_AnimalActionPerformed

    private void BT_regresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BT_regresoActionPerformed
        if (DeMapas) {//Si FrmMapas abrio FrmAnimales devolverse a mapas
            FrmMapas a = new FrmMapas(sesion);
            a.setVisible(true);
        } else {//De lo contrario devolverse al mdi principal
            MDI_principal a = new MDI_principal(sesion);
            a.setVisible(true);
        }
        dispose();
    }//GEN-LAST:event_BT_regresoActionPerformed

    private void CB_RazaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CB_RazaActionPerformed
        String item = (String) CB_Raza.getSelectedItem();//Guardo el elemento seleccionado en el combobox raza en una variable
        if (item != null && !(item.contains("Seleccione"))) {//Si la seleccion es un placeholder, ignorarla
            String[] ItemPartes = ExpandirCodRef(item, "Raza");//Expande el codigo de la seleccion de la raza 
                                                            //Para incluir la especie, esto retorna un vector
            item = String.join(".", ItemPartes[0], ItemPartes[1]);//uno el vector en una cadena
            if (CB_Raza.getItemAt(0).contains("Seleccione")) {
                CB_Raza.removeItemAt(0);//Remueve el placeholder
            }
            CB_Raza.setForeground(Color.BLACK);//Cambio el color a negro
            CB_Animal.setModel(new DefaultComboBoxModel<>(makeCmbList(item)));//Lleno el combobox animal
                                                                            //con el vector que retorna makeCmbList
        }
    }//GEN-LAST:event_CB_RazaActionPerformed

    private void CB_EspecieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CB_EspecieActionPerformed
        String item = (String) CB_Especie.getSelectedItem();//Guardo el elemento seleccionado en el combobox especie en una variable
        if (item != null && !(item.equalsIgnoreCase("Seleccione Especie"))) {//Si la seleccion es un placeholder, ignorarla
            if (CB_Especie.getItemAt(0).contains("Seleccione")) {
                CB_Especie.removeItemAt(0);//Remueve el placeholder
            }
            CB_Especie.setForeground(Color.BLACK);//Cambio el color a negro
            CB_Raza.setModel(new DefaultComboBoxModel<>(makeCmbList(item)));//Lleno el combobox raza
            //con el vector que retorna makeCmbList
            HabilitarPaneles(false);
            CB_Animal.removeAllItems();//Limpio el combobox de animales
            CB_Animal.setForeground(Color.LIGHT_GRAY);//Color gris del placeholder de animales
            CB_Animal.addItem("Seleccione Raza");//Agrego placeholder de animales
        }
    }//GEN-LAST:event_CB_EspecieActionPerformed

    private void BT_cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BT_cerrarActionPerformed
        //nos arroja una ventana de confirmacion al usuario para determinar si desea seguir 
        int confirmar = JOptionPane.showConfirmDialog(rootPane, "Estas seguro que deseas salir del programa?");
        if (JOptionPane.OK_OPTION == confirmar) {// si es afirmativa la respuesta 
           dispose();// cerrar la ventana actual
            MDI_principal a = new MDI_principal(sesion);// abre y hace visible el mdi principal con todosd los metodos establecidos en infosesion
            a.setVisible(true);// hace visible la ventana
            
        } else if (JOptionPane.CANCEL_OPTION == confirmar) {
            return;// si es negativa entonces vuekve a la ventana establecida o actual
        }
    }//GEN-LAST:event_BT_cerrarActionPerformed

    private void CkB_AlimentacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CkB_AlimentacionActionPerformed
        AgregarMant("Alimentacion"); //añade al método agregarmant el string "Alimentación"
    }//GEN-LAST:event_CkB_AlimentacionActionPerformed

    private void CkB_VitaminasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CkB_VitaminasActionPerformed
        AgregarMant("Vitaminas"); //añade al método agregarmant el string "Vitaminas"
    }//GEN-LAST:event_CkB_VitaminasActionPerformed

    private void CkB_LimpiezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CkB_LimpiezaActionPerformed
        AgregarMant("Limpieza"); //añade al método agregarmant el string "Limpieza"
    }//GEN-LAST:event_CkB_LimpiezaActionPerformed

    private void BT_GuardarObservacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BT_GuardarObservacionesActionPerformed
        String estado = "";
        if(!((String)CmB_Estado.getSelectedItem()).contains("Seleccione")){
            estado = (((String)CmB_Estado.getSelectedItem()).split("\\.", 2))[1];//Le quito los numeros y el punto a la seleccion
        } 
        String msg = "Mantenimiento Realizado \n"//Creo una cadena con toda la info del mantenimiento
                + "\tEstado del animal: "+estado+"\n"
                + "\tMantenimientos Realizados:\n";
        for (String Mant : MantRealizado){
            msg+="\t\t-"+Mant+"\n";//Incluyendo los checkbox que fueron señalados
        }
        msg+= "\tObservaciones Extra:\n"
                + "\t\t"+TA_Observaciones.getText();//Y el textarea con la info extra
        sesion.log(msg);//Logeo la cadena a la bitacora
        HabilitarPaneles(false);//Escondo el panel
    }//GEN-LAST:event_BT_GuardarObservacionesActionPerformed

    private void BT_ImprimirDiagnosticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BT_ImprimirDiagnosticoActionPerformed
        String diagnostico = TA_Diagnostico1.getText();// obtengo la informacion dentro del text area
        String animal = LB_NombreFill.getText();// nombre del animal (get)
        sesion.log("Diagnostico del animal:"+animal+" \n\t"+diagnostico);// manda a la bitácora del admin el diasgnostico establecido y el animal
        HabilitarPaneles(false);//Escondo el panel
        try {
            PrintReport(diagnostico);// imprime el reporte del diagnostico
        } catch (JRException ex) {
            Logger.getLogger(FrmAnimal.class.getName()).log(Level.SEVERE, null, ex);// error del no cumpliemto del try si el diagnostico no tiene los parametros completos y trae este error
        }
    }//GEN-LAST:event_BT_ImprimirDiagnosticoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmAnimal(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BT_GuardarObservaciones;
    private javax.swing.JButton BT_ImprimirDiagnostico;
    private javax.swing.JButton BT_cerrar;
    private javax.swing.JButton BT_regreso;
    private javax.swing.JComboBox<String> CB_Animal;
    private javax.swing.JComboBox<String> CB_Especie;
    private javax.swing.JComboBox<String> CB_Raza;
    private javax.swing.JCheckBox CkB_Alimentacion;
    private javax.swing.JCheckBox CkB_Limpieza;
    private javax.swing.JCheckBox CkB_Vitaminas;
    private javax.swing.JComboBox<String> CmB_Estado;
    private javax.swing.JLabel LB_Animal;
    private javax.swing.JLabel LB_Edad;
    private javax.swing.JLabel LB_EdadFill;
    private javax.swing.JLabel LB_Especie;
    private javax.swing.JLabel LB_Estado;
    private javax.swing.JLabel LB_Estatura;
    private javax.swing.JLabel LB_EstaturaFill;
    private javax.swing.JLabel LB_Extincion;
    private javax.swing.JLabel LB_ExtincionFill;
    private javax.swing.JLabel LB_Fondo;
    private javax.swing.JLabel LB_ImgAnimal;
    private javax.swing.JLabel LB_NombreFill;
    private javax.swing.JLabel LB_Observaciones;
    private javax.swing.JLabel LB_Peso;
    private javax.swing.JLabel LB_PesoFill;
    private javax.swing.JLabel LB_Raza;
    private javax.swing.JLabel LB_Sexo;
    private javax.swing.JLabel LB_SexoFill;
    private javax.swing.JLabel LB_Veneno;
    private javax.swing.JLabel LB_VenenoFill;
    private javax.swing.JTabbedPane Mantenimiento;
    private javax.swing.JPanel Panel_Diagnostico;
    private javax.swing.JPanel Panel_Mantenimiento;
    private javax.swing.JScrollPane SPanel_Mantenimiento;
    private javax.swing.JScrollPane Spane_TA_Diag;
    private javax.swing.JTextArea TA_Diagnostico1;
    private javax.swing.JTextArea TA_Observaciones;
    private javax.swing.JPanel TPanel_Diagnostico;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
