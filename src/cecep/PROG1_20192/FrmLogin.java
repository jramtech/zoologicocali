package cecep.PROG1_20192;           //Identifica donde esta guandado el formulario
import java.awt.Color;               //Tiene como funcion de cambiar la apariencia del objeto
import java.awt.Image;               //Contiene clases e interfaces para manipular imágenes.
import java.awt.event.ActionEvent;   //Indica que ocurrió una acción
import java.awt.event.ActionListener;   //Recibe un eventos de acción
import java.awt.event.KeyEvent;        //Es un evento que indica cuando se  produce una pulsación de tecla en un componente.
import java.text.SimpleDateFormat;     //se usa para analizar y formatear fechas de acuerdo con un patrón de formato que usted especifique. 
import java.util.Calendar;             //Posee una gran cantidad de métodos para operar, consultar y modificar las propiedades de una fecha.
import java.util.Date;                 //La clase Date del paquete java.util implementa una interfaz serializable, clonable y comparable
import javax.swing.ImageIcon;          //Una implementación de la interfaz de iconos que pinta iconos de imágenes. Las imágenes que se crean a partir de una URL, nombre de archivo o matriz de bytes.
import javax.swing.JOptionPane;        //Esta clase hace realmente fácil el mostrar ventanas standards para mostrar y pedir información a los usuarios.
import javax.swing.Timer;             //Dispara uno o más eventos de acción a intervalos especificados.

/**
 *
 * @author Alejandra, Danna
 */
public class FrmLogin extends javax.swing.JFrame {

    
    //llamar de manera publica las clases y los metodos
    public static InfoSesion Usuario_sesion;
    public static InfoSesion sesion_antigua;

    public FrmLogin(InfoSesion sesion) {
        initComponents();
        //declaracion de las variables
        sesion_antigua=sesion;
        this.setLocationRelativeTo(null);
        //Cada vez cuando pongas en maus en obgeto, te sale una pequeña descripcion de lo que debes hacer
        PF_Contrasena.setToolTipText("Digite la contraseña");
        TF_Usuario.setToolTipText("Digite el nombre de usuario");
        BT_Cerrar.setToolTipText("Cerrar Programa");
        TF_Usuario.setText("Usuario");
        TF_Usuario.setForeground(Color.GRAY);
        ImageIcon imagen = new ImageIcon(findimg("/Imagenes/AvatarLogin.png"));//selecciona la imagen y la instancian en el constructor 
        //esta parte del codigo es para asignarles las propiedades al Jlabel como la altura y el ancho
        ImageIcon Normal = new ImageIcon(imagen.getImage().getScaledInstance(LB_ImgAvatar.getWidth(), LB_ImgAvatar.getHeight(), Image.SCALE_DEFAULT));
        LB_ImgAvatar.setIcon(Normal);

        ImageIcon Fondbloq = new ImageIcon(findimg("/Imagenes/FondoFrmLogin.png"));
        ImageIcon Fondbloq1 = new ImageIcon(Fondbloq.getImage().getScaledInstance(LB_ImgFondo.getWidth(), LB_ImgFondo.getHeight(), Image.SCALE_DEFAULT));
        LB_ImgFondo.setIcon(Fondbloq1);

        Date sistFecha = new Date();//creamos e instanciamos una variable de tipo date con la fecha actual
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/YYYY");//Formato de la fecha
        LB_Fecha.setText(formato.format(sistFecha));//Agregamos la fecha al label fecha con el formato especificado

        //Hora del sistema
        Timer tiempo = new Timer(100, new FrmLogin.horas());  //El timer cuenta los segundos o el tiempo
        tiempo.start();//Esta parte es para que se ejecute la Hora 
        TF_Usuario.selectAll();
    }

    private String findimg(String ruta){
        //Toma la runata y la combierte en cadena y a trae la ruta 
        String fichero = (getClass().getResource(ruta)).toString();
        if (fichero.contains("file:")) {
            fichero = fichero.replaceAll("file:", "");
        }//remover "file:" de la ruta del archivo
        return fichero;
    }
    class horas implements ActionListener {

        
        
        public void actionPerformed(ActionEvent e) {
            //Dar la hora en tiempo real 
            Date sistHora = new Date();//Aqui se crea y se instancia una variable de tipo date 
            String pmAm = "hh:mm a";// El formato de la fecha
            //Se le asigna.
            SimpleDateFormat format = new SimpleDateFormat(pmAm); //Modelo de la forma que le queramos 
            
            Calendar hoy = Calendar.getInstance();
            LB_Hora.setText(String.format(format.format(sistHora), hoy));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BT_Cerrar = new javax.swing.JButton();
        LB_Fecha = new javax.swing.JLabel();
        LB_Hora = new javax.swing.JLabel();
        LB_ImgAvatar = new javax.swing.JLabel();
        LB_Usuario = new javax.swing.JLabel();
        TF_Usuario = new javax.swing.JTextField();
        LB_Contrasena = new javax.swing.JLabel();
        PF_Contrasena = new javax.swing.JPasswordField();
        LB_IncorrectUser = new javax.swing.JLabel();
        LB_ImgFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImages(null);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BT_Cerrar.setBackground(new java.awt.Color(255, 255, 255));
        BT_Cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/CerrarIcono.png"))); // NOI18N
        BT_Cerrar.setFocusable(false);
        BT_Cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BT_CerrarActionPerformed(evt);
            }
        });
        getContentPane().add(BT_Cerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 0, 20, 20));

        LB_Fecha.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(LB_Fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 40, 134, 15));

        LB_Hora.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(LB_Hora, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 340, 74, 13));
        getContentPane().add(LB_ImgAvatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 50, 110, 110));

        LB_Usuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LB_Usuario.setText("Usuario");
        getContentPane().add(LB_Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 250, 70, 20));

        TF_Usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TF_UsuarioActionPerformed(evt);
            }
        });
        TF_Usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TF_UsuarioKeyPressed(evt);
            }
        });
        getContentPane().add(TF_Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 250, 150, -1));

        LB_Contrasena.setBackground(new java.awt.Color(0, 0, 0));
        LB_Contrasena.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LB_Contrasena.setText("Contraseña");
        getContentPane().add(LB_Contrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 290, 90, 20));

        PF_Contrasena.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PF_ContrasenaKeyPressed(evt);
            }
        });
        getContentPane().add(PF_Contrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 290, 150, -1));

        LB_IncorrectUser.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(LB_IncorrectUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 330, 290, 30));
        getContentPane().add(LB_ImgFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 650, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BT_CerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BT_CerrarActionPerformed
       //declaramos la variable de confirmar si el usuaario quiere salir del programa
        int confirmar = JOptionPane.showConfirmDialog(rootPane, "Estas seguro que deseas salir del programa?");
        if (JOptionPane.OK_OPTION == confirmar) {
            System.exit(0);
        } else if (JOptionPane.CANCEL_OPTION == confirmar) {
            return;
        }
    }//GEN-LAST:event_BT_CerrarActionPerformed

    private void TF_UsuarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TF_UsuarioKeyPressed
            
        //Cuando escribimos el usuario y le damos enter dos manda esta condicion 
        //si se cumple para al siguiente
        if (evt.getKeyChar() == evt.VK_ENTER) {
            PF_Contrasena.requestFocus();
        } else {
            if (!PF_Contrasena.getText().equals("Usuario")){
                TF_Usuario.setForeground(Color.BLACK);
            }
        }

    }//GEN-LAST:event_TF_UsuarioKeyPressed

    private void PF_ContrasenaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PF_ContrasenaKeyPressed
       //validacion de los usurios
        String usuario = TF_Usuario.getText();
        String contraseña = PF_Contrasena.getText();
        if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
            for (String[] elemento : Matrices.Usuarios) {
                if (elemento[1].equalsIgnoreCase(usuario) && elemento[2].equalsIgnoreCase(contraseña)) {
                    Usuario_sesion = new InfoSesion(elemento);
                    if(sesion_antigua!=null)Usuario_sesion.setLog(sesion_antigua.getLog());
                    Usuario_sesion.log("Login");
                    try {
                        FrmProgBar barra = new FrmProgBar(Usuario_sesion);
                        barra.setVisible(true);
                        
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    dispose();
                }else {
                    LB_IncorrectUser.setText("¡Usuario o contraseña incorrectos!");
                    LB_IncorrectUser.setVisible(true);
                    LB_IncorrectUser.setForeground(Color.red);
                    PF_Contrasena.setText("");
                    PF_Contrasena.setFocusable(true);
                }  
            }
        }
    }//GEN-LAST:event_PF_ContrasenaKeyPressed

    private void TF_UsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TF_UsuarioActionPerformed
        TF_Usuario.setText("");
    }//GEN-LAST:event_TF_UsuarioActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmAnimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FrmLogin(null).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BT_Cerrar;
    private javax.swing.JLabel LB_Contrasena;
    private javax.swing.JLabel LB_Fecha;
    private javax.swing.JLabel LB_Hora;
    private javax.swing.JLabel LB_ImgAvatar;
    private javax.swing.JLabel LB_ImgFondo;
    private javax.swing.JLabel LB_IncorrectUser;
    private javax.swing.JLabel LB_Usuario;
    private javax.swing.JPasswordField PF_Contrasena;
    private javax.swing.JTextField TF_Usuario;
    // End of variables declaration//GEN-END:variables
}
