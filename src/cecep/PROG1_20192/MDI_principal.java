
package cecep.PROG1_20192;
import com.sun.jna.Native;        //Proporciona gestión de recursos de bibliotecas nativas. Una instancia de esta clase corresponde a una única biblioteca nativa cargada. 
import com.sun.jna.NativeLibrary; //proporciona varias utilidades para operaciones nativas.
import java.awt.Image;            //Contiene clases e interfaces para manipular imágenes.
import javax.swing.ImageIcon;     //Una implementación de la interfaz de iconos que pinta iconos de imágenes. 
import uk.co.caprica.vlcj.binding.LibVlc;// llamamos las librerias para tomar las propiedades del vlc
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;// me da loas propiedades de la reproduccion del video
import uk.co.caprica.vlcj.runtime.RuntimeUtil;// establece como soprte el vlc como reproductor

public class MDI_principal extends javax.swing.JFrame {
  //Declaracion de las variables
    private final EmbeddedMediaPlayerComponent player = new EmbeddedMediaPlayerComponent();// declaramos a player como un reproductor
    int c = 0, f = 0; // variable spra establecer la funcion del tooglebutton
     public static InfoSesion sesion;

    public MDI_principal(InfoSesion sesion) {
        initComponents();
        this.sesion = sesion;
        setLocationRelativeTo(null); //centramos la ventana en la pantalla
        panel_pri.add(player);//al panel principal agregamos un objeto que es el reproductor
        player.setSize(panel_slider.getSize());// estableemos que el panel que contiene el slider es del mismo tamaño
        player.setVisible(true);// sea posible la visualizacion del objeto

        String fichero = (getClass().getResource("/Imagenes/GIF3.gif")).toString(); //llamamos de los recursos del programa el gif del slider
        if (fichero.contains("file:")) { //si obtenemos el archivo, que remueva el file y lo reproduzca
            fichero = fichero.replaceAll("file:", "");
        }//remover "file:" de la ruta del archivo

        ImageIcon Fondbloq = new ImageIcon(fichero); //toma la ruta al fichero
        ImageIcon Fondbloq1 = new ImageIcon(Fondbloq.getImage().getScaledInstance(lbl_slider.getWidth(), lbl_slider.getHeight(), Image.SCALE_DEFAULT));
        lbl_slider.setIcon(Fondbloq1);// establece que se adapte al label y por ultimo que lo punga como icono

       
  //Llama la imagen dependiendo del usuario
        if (sesion.getCargo().equals("Veterinario")) {
            ImageIcon img = new ImageIcon("src/Imagenes/FondoMdiVeterinario.png");       //selecciona la imagen y la instancian en el constructor 
        //esta parte del codigo es para asignarles las propiedades al Jlabel como la altura y el ancho
            ImageIcon ajust = new ImageIcon(img.getImage().getScaledInstance(lbl_fondo.getWidth(), lbl_fondo.getHeight(), Image.SCALE_DEFAULT));
            lbl_fondo.setIcon(ajust);//Inicia la imagen
        } else if (sesion.getCargo().equals("Cuidador")) {
            ImageIcon img = new ImageIcon("src/Imagenes/FondoMdiCuidador.png");       //selecciona la imagen y la instancian en el constructor 
             //esta parte del codigo es para asignarles las propiedades al Jlabel como la altura y el ancho
            ImageIcon ajust = new ImageIcon(img.getImage().getScaledInstance(lbl_fondo.getWidth(), lbl_fondo.getHeight(), Image.SCALE_DEFAULT));
            lbl_fondo.setIcon(ajust);//Inicia la Imagen 

        }

        ImageIcon icon1 = new ImageIcon("src/Imagenes/icon_play.png");// damos el icono del toogle de play al iniciar
        tggl_play.setIcon(icon1);

        ImageIcon icon2 = new ImageIcon("src/Imagenes/icon_sin_sonido.png");// damos el icono del toogle de parar al iniciar
        tggl_sonido.setIcon(icon2);

    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktopPane = new javax.swing.JDesktopPane();
        btn_animales = new javax.swing.JButton();
        btn_mapa = new javax.swing.JButton();
        panel_pri = new javax.swing.JPanel();
        panel_slider = new javax.swing.JPanel();
        lbl_slider = new javax.swing.JLabel();
        tggl_sonido = new javax.swing.JToggleButton();
        tggl_play = new javax.swing.JToggleButton();
        lbl_fondo = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        Animal_item = new javax.swing.JMenuItem();
        Mapa_item = new javax.swing.JMenuItem();
        CerrarSesion = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        btn_animales.setBackground(new java.awt.Color(0, 0, 51));
        btn_animales.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/BotonAnimal1.png"))); // NOI18N
        btn_animales.setBorder(null);
        btn_animales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_animalesActionPerformed(evt);
            }
        });
        desktopPane.add(btn_animales);
        btn_animales.setBounds(110, 210, 90, 70);

        btn_mapa.setBackground(new java.awt.Color(0, 0, 51));
        btn_mapa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/BotonMapa.png"))); // NOI18N
        btn_mapa.setBorder(null);
        btn_mapa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_mapaActionPerformed(evt);
            }
        });
        desktopPane.add(btn_mapa);
        btn_mapa.setBounds(110, 320, 90, 70);

        javax.swing.GroupLayout panel_sliderLayout = new javax.swing.GroupLayout(panel_slider);
        panel_slider.setLayout(panel_sliderLayout);
        panel_sliderLayout.setHorizontalGroup(
            panel_sliderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_sliderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_slider, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
        );
        panel_sliderLayout.setVerticalGroup(
            panel_sliderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_sliderLayout.createSequentialGroup()
                .addComponent(lbl_slider, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addContainerGap())
        );

        tggl_sonido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tggl_sonidoActionPerformed(evt);
            }
        });

        tggl_play.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tggl_playActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_priLayout = new javax.swing.GroupLayout(panel_pri);
        panel_pri.setLayout(panel_priLayout);
        panel_priLayout.setHorizontalGroup(
            panel_priLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_priLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_priLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tggl_sonido, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tggl_play, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panel_priLayout.createSequentialGroup()
                .addComponent(panel_slider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panel_priLayout.setVerticalGroup(
            panel_priLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_priLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tggl_sonido, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tggl_play, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
                .addComponent(panel_slider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        desktopPane.add(panel_pri);
        panel_pri.setBounds(250, 70, 470, 410);
        desktopPane.add(lbl_fondo);
        lbl_fondo.setBounds(0, 0, 920, 500);

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        desktopPane.add(jPanel3);
        jPanel3.setBounds(0, 0, 0, 0);

        fileMenu.setMnemonic('f');
        fileMenu.setText("El zoo");

        Animal_item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        Animal_item.setMnemonic('o');
        Animal_item.setText("Animales");
        Animal_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Animal_itemActionPerformed(evt);
            }
        });
        fileMenu.add(Animal_item);

        Mapa_item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        Mapa_item.setMnemonic('a');
        Mapa_item.setText("Mapa");
        Mapa_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Mapa_itemActionPerformed(evt);
            }
        });
        fileMenu.add(Mapa_item);

        CerrarSesion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        CerrarSesion.setMnemonic('x');
        CerrarSesion.setText("Cerrar Sesion");
        CerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CerrarSesionActionPerformed(evt);
            }
        });
        fileMenu.add(CerrarSesion);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(desktopPane, javax.swing.GroupLayout.PREFERRED_SIZE, 919, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_animalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_animalesActionPerformed
        FrmAnimal a = new FrmAnimal(sesion);
        a.setVisible(true);//al pulsar el boton de animales nos llevara al frm_animal para filtrar el animal que desea el usuario
        player.getMediaPlayer().stop();// al presionarlo se para el video
        player.getMediaPlayer().setVolume(0);// y se pone en mute
        dispose();// cierre de la ventana actual
    }//GEN-LAST:event_btn_animalesActionPerformed

    private void btn_mapaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_mapaActionPerformed
        FrmMapas a = new FrmMapas(sesion);//al pulsar el boton de mapas nos llevara al frm_mapas para seleccionar la zona deseada ya si filtrar el animal
        a.setVisible(true);
        player.getMediaPlayer().stop();// al presionarlo se para el video
        player.getMediaPlayer().setVolume(0);// y se pone en mute
        dispose();// cierre de la ventana actual
    }//GEN-LAST:event_btn_mapaActionPerformed

    private void CerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CerrarSesionActionPerformed
        FrmLogin a = new FrmLogin(sesion);// al pulsar el item cerrar seion nos permitira cambiar de usuairio en el login
        a.setVisible(true);
        player.getMediaPlayer().stop();// al presionarlo se para el video
        player.getMediaPlayer().setVolume(0);// y se pone en mute
        dispose();// cierre de la ventana actual

    }//GEN-LAST:event_CerrarSesionActionPerformed

    private void Mapa_itemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Mapa_itemActionPerformed
        FrmMapas a = new FrmMapas(sesion);//al pulsar el item de mapas nos llevara al frm_mapas para seleccionar la zona deseada ya si filtrar el animal
        a.setVisible(true);
        player.getMediaPlayer().stop();// al presionarlo se para el video
        player.getMediaPlayer().setVolume(0);// y se pone en mute
        dispose();// cierre de la ventana actual
    }//GEN-LAST:event_Mapa_itemActionPerformed

    private void Animal_itemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Animal_itemActionPerformed
        FrmAnimal a = new FrmAnimal(sesion);//al pulsar el item de animales nos llevara al frm_animal para filtrar el animal que desea el usuario
        a.setVisible(true);
        player.getMediaPlayer().stop();// al presionarlo se para el video
        player.getMediaPlayer().setVolume(0);// y se pone en mute
        dispose();// cierre de la ventana actual
    }//GEN-LAST:event_Animal_itemActionPerformed

    private void tggl_playActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tggl_playActionPerformed

        if (c == 0) { // si el boton tiene el valor de  asignado entonces reproducira el video
            player.getMediaPlayer().playMedia("src/video/ZOO video.mp4");
            c = 1;// la variable cambiara a 1
            ImageIcon icon1 = new ImageIcon("src/Imagenes/icon_play.png");
            tggl_play.setIcon(icon1);//se pondra la imagen de play activa
            sesion.log("video reproduciendo");// tomamos bitacora del admi en cada reproduccion o la detenida del video
        } else if (c == 1) {
            player.getMediaPlayer().stop();//si c == 1 entonces se parar el video
            c = 0;// volvera al valor inicial para volver a realizar las validaciones y poderlo reproducir y parar las veces que se necesiten o desean
            ImageIcon icon = new ImageIcon("src/Imagenes/icon_parar.png");// cambiamos el icono a parar
            tggl_play.setIcon(icon);
            sesion.log("video detenido");// tomamos bitacora del admi en cada reproduccion o la detenida del video
        }
    }//GEN-LAST:event_tggl_playActionPerformed

    private void tggl_sonidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tggl_sonidoActionPerformed
        if (f == 0) {// if la variable es == 0 entonces el video se pondra en mute
            player.getMediaPlayer().setVolume(0);//Coloca el volumen del video en 0 
            f = 1;// cambia el valor de la variable
            ImageIcon icon1 = new ImageIcon("src/Imagenes/icon_sin_sonido.png");
            tggl_sonido.setIcon(icon1);// cambiamos el icono del toogle
        } else if (f == 1) {// si la variable entra como 1 entonces se reestableceraa el volumen del video 
            player.getMediaPlayer().setVolume(100);//Se le asigna un volumen de 100 al sonido 

            ImageIcon icon = new ImageIcon("src/Imagenes/icon_sonido.png");// se cambiara el icono del toogle
            tggl_sonido.setIcon(icon);//agregamosel icono
            f = 0;// establecemos la variable para que podamos volver a la anterior validacion y es usuario pueda escoger las veces que quiera poner o no sonido al video
        }
    }//GEN-LAST:event_tggl_sonidoActionPerformed

    static {
        //componentes que traen las librerias y dan acceso a el vlc como reproductor del objeto creado en el constructor y asi ver el archivo
        NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(), "C:/Program Files/VideoLAN/VLC/");
        Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MDI_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MDI_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MDI_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MDI_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MDI_principal(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Animal_item;
    private javax.swing.JMenuItem CerrarSesion;
    private javax.swing.JMenuItem Mapa_item;
    private javax.swing.JButton btn_animales;
    private javax.swing.JButton btn_mapa;
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lbl_fondo;
    private javax.swing.JLabel lbl_slider;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JPanel panel_pri;
    private javax.swing.JPanel panel_slider;
    private javax.swing.JToggleButton tggl_play;
    private javax.swing.JToggleButton tggl_sonido;
    // End of variables declaration//GEN-END:variables

}
