package cecep.PROG1_20192;
import java.awt.Color;              //Proporciona el conjunto de clases para facilitar la utilización de animaciones basadas en la transición
import java.awt.Image;              //Contiene clases e interfaces para manipular imágenes.
import java.awt.event.ActionEvent;   //Indica que ocurrió una acción
import java.awt.event.ActionListener;  //Recibe un eventos de acción
import javax.swing.ImageIcon;    ////Una implementación de la interfaz de iconos que pinta iconos de imágenes. Las imágenes que se crean a partir de una URL, nombre de archivo o matriz de bytes.
import javax.swing.Timer;        //Dispara uno o más eventos de acción después de un retraso especificado
import javax.swing.UIManager;    //Gestiona el aspecto actual y el conjunto de aspectos disponibles

public class FrmProgBar extends javax.swing.JFrame {
    //Declaracion de las variables
    private Timer t;
    private final ActionListener ac;
    private int i = 0;

    public FrmProgBar(InfoSesion sesion) {
        
        initComponents();
        setLocationRelativeTo(null);// Le da la ubicación dentro de la pantalla al formulario
        //Toma la runata y la combierte en cadena y a trae la ruta 
        String fichero = (getClass().getResource("/Imagenes/GIF_BarraProgreso.gif")).toString();
        if (fichero.contains("file:")) {
            fichero = fichero.replaceAll("file:", "");
        }//remover "file:" de la ruta del archivo
        
        ImageIcon Fondbloq = new ImageIcon(fichero);//Se llama la imagen del fichero
        //Esta linea de codigo es pata darles las propiedades al Jlabel, dandole asi el alto y el ancho
        ImageIcon Fondbloq1 = new ImageIcon(Fondbloq.getImage().getScaledInstance(LB_ImgFondo.getWidth(), LB_ImgFondo.getHeight(), Image.SCALE_DEFAULT));
        LB_ImgFondo.setIcon(Fondbloq1);  //Aqui inicia 

        //Esta line sirve para cambiarle el color a la barra de progreso
        UIManager.put("barraprogreso.background", Color.CYAN);

        ac = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                i++;//Contador 
                barraprogreso.setValue(i);     //le asignamos un valor a "barrarpogreso"  
                if (barraprogreso.getValue() == 100) { //comparamos con una condicion si el ususario equivale a "Visitante"
                    dispose(); // Cierra la instancia
                    t.stop();  //Detiene la barra de progreso
                    //Validacion de los formulario dependiendo del tipo de usrio que elijan
                    if (sesion.getCargo().equals("Veterinario")||sesion.getCargo().equals("Cuidador")){
                        MDI_principal a = new MDI_principal(sesion);  //Se crea un objeto de tipo MDI con los parametros de sesion 
                        a.setVisible(true); //Lo visualizo
                    }else if(sesion.getCargo().equals("Administrador")){ //comparamos con una condicion si el ususario equivale a "Administrador"
                        FrmAdministrador frmAdmi=new FrmAdministrador(sesion); //La barra de bogreso mira quien incio
                        frmAdmi.setVisible(true);//Lo visualiza
                    }
                    
                }
            }
        };
        t = new Timer(46, ac);// se instancia el timer con los parametros para que cada vez que se ejecute el evento action Listener avance 46 milisegundos y así completar a los 4 segundos el 100 % d ela barra
        t.start();//Se inicia el timer
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barraprogreso = new javax.swing.JProgressBar();
        LB_ImgFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setSize(new java.awt.Dimension(720, 480));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        barraprogreso.setBackground(new java.awt.Color(0, 153, 153));
        barraprogreso.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        getContentPane().add(barraprogreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 390, 630, 40));
        getContentPane().add(LB_ImgFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 720, 480));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmProgBar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmProgBar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmProgBar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmProgBar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmProgBar(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LB_ImgFondo;
    private javax.swing.JProgressBar barraprogreso;
    // End of variables declaration//GEN-END:variables
}
