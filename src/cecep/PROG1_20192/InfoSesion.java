
package cecep.PROG1_20192;

import java.time.LocalDateTime;            // Esta librería me permite el uso de la función de formato del tiempo
import java.time.format.DateTimeFormatter; //Esta librería me permite el uso de diferentes formato de fecha 
import java.util.ArrayList;                // Esta librería me permite el uso de arrayList o listas dinámicas 

/**
 *
 * @author joe
 */
public class InfoSesion {
     
    //Declaracion de las variables
    private final String cod_usuario;
    private final String usuario;
    private final String nombre_completo;
    private final String Tipo_empleado;
    private final String especializacion;
    //instancio una arraylist de tipo privada 
    private ArrayList log = new ArrayList();//maybe it needs to be final

    public InfoSesion(String[] cod) {  // inicio del metodo
        cod_usuario = cod[0];  //instanciar la variable y de doy un valor en este caso 0
        usuario = cod[1];
        String[] empleado=null;  // inicio la variable empleado tipo string y le doy un valor nulo 
        String especializacion=null;  // inicio la variable tipo string y le doy un valor nulo 
        for (String[] elemento : Matrices.empleados) { // le asigno los valores de matriz empleado a elemento 
            if (elemento[0].substring(4, 6).equalsIgnoreCase(cod_usuario)) {
                empleado = elemento;
         // en este ciclo se toman dos valores del empleado para hacer uso de la llave compuesta 
            }
        }
        for (String[] elemento : Matrices.veterinario) { // le asigno los valores de matriz veterinario a elemento 
            if (elemento[0].substring(4, 6).equalsIgnoreCase(cod_usuario)) {
                empleado = elemento;
                // en este ciclo se toman dos valores de la matriz veterinario  para hacer uso de la llave compuesta 

            }
        }
        for (String[] cargo : Matrices.cargos){
            String cod_cargo = empleado[0].substring(2, 4);  // iniciamos una la variable cod_cargo y lo comparamos com dos valores del arrayList empleado 
            if(cargo[0].equals(cod_cargo)){
                especializacion=cargo[1];
            }
        }
        this.especializacion=especializacion;
        nombre_completo = empleado[1] +" "+ empleado[2]; //comparó la variable nombre_completo con la concatenación de empleado[1] y empleado[2]
        Tipo_empleado = NombreCargo(Integer.parseInt(empleado[0].substring(2, 4)));
        //comparó  el tipo de empleado con el nombre del cargo teniendo en cuenta el código desde el segundo hasta el cuarto dígito 

    };
    
    private String NombreCargo(int CodCargo){ // iniciamos el método que tiene como parámetro un entero 
        String NombreCargo = "";  // Inició una variable tipo string sin valor asignado 
        if (CodCargo<=10){        //si el codCargo es menor o igual a 10 entonces continúe 
            NombreCargo="Veterinario";   // el nombre del cargo es igual a veterinario
        } else if (CodCargo>10&&CodCargo<14){
            NombreCargo="Cuidador"; //si el codigo del cargo es mayor a 10 y menor a 14 entonces el nombre del cargo es igual a cuidador 
        } else if (CodCargo==14){
            NombreCargo="Administrador";//si el codigo del cargo es exactamente igual a 14 entonces el nombre de cargo es igual administrador
        }
        return NombreCargo;  //retorne el valor de nombreCargo
    }

    public String getCargo() { // inició el metodo getCargo 
        return Tipo_empleado;  // que me retorne el tipo de empleado 
    }

    public String getCod_usuario() {
        return cod_usuario;  //Captua los código de usuario y los retorna 
    }

    public String getNombre_completo() {
        return nombre_completo; //Captura el nombre completo  y los retorna
    }
    
    public void log(String msg){
        //Capturamos la fecha de incio de secion
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now);
        //concateno la informacion 
        log.add("\n"+date+" -- Usuario: "+usuario+", Nombre: "+nombre_completo+", Cargo: "+Tipo_empleado+"\n"
                + "Accion: "+msg);
    }
 //me permite modificar el método desde cualquier otro formulario cualquiera de los tres subsiguientes dependiendo de la información
    public ArrayList getLog() {
        return log;
    }

    
    public void setLog(ArrayList log) {
        this.log = log;
    }
    
    public String getEspecializacion() {
        return especializacion;
    }
    
    
    
}
