package cecep.PROG1_20192;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Juan G
 */
public class Matrices {

    //Codigo, especie
    static String[][] especie = {
        {"01", "Vertebrados"},
        {"02", "Invertebrados"}
    };

    //Codigo, raza
    static String[][] razas = {
        {"0101", "Aves"},
        {"0102", "Mamiferos"},
        {"0103", "Anfibios"},
        {"0104", "Reptiles"},
        {"0105", "Peces"},
        {"0201", "Insectos"}
    };

    //Codigo: **Especie, **Raza, **animal, **hábitat, **alimentación
    //[0]Codigo, [1]Animal, [2]Nombre_cientifico, [3]Peso, [4]Estatura, [5]Fec_Nac, [6]Sexo, [7]Venenoso, [8]Peligro Extincion
    public static String[][] animales = {
        {"0101010222", "Avestruz", "Struthio camelus", "180 Kg", "2,45 m", "15/12/2019", "M", "No", "No"},
        {"0102020322", "Babuino", "Papio Anubis", "30 Kg", "90 cm", "25/01/1980", "F", "No", "No"},
        {"0104030703", "Boa Arborícola", "Corallus Hortulanus", "12 Kg", "1,7 m", "5/04/2000", "M", "No", "No"},
        {"0104040716", "Boa Constrictor", "Boa Constrictor Constrictor", "13 Kg", "3 m", "6/08/1999", "F", "No", "No"},
        {"0104050716", "Cabeza de Candado", "Bothriechis schlegelii", "1 kg", "95 cm", "5/01/2001", "M", "Si", "No"},
        {"0104060412", "Caiman", "Caiman Crocodilus", "40 Kg", "1,8 m", "8/10/1997", "F", "No", "Si"},
        {"0102070917", "Canguro Rojo", "Macropus Rufus", "90 Kg", "1,6 m", "17/03/2003", "M", "No", "No"},
        {"0102081018", "Capibara", "Hydrochoerus Hydrochaeris", "65 Kg", "55 cm", "02/02/2001", "F", "No", "Si"},
        {"0105090105", "Carpa Dorada", "Carassius Auratus Auratus", "3 Kg", "60 cm", "17/11/2012", "M", "No", "Si"},
        {"0105100117", "Carpa Koi", "Cyprinus Carpio Koi", "40 Kg", "55 cm", "01/09/2012", "F", "No", "No"},
        {"0104110102", "Cascabel", "Crotalus", "4 Kg", "2,5 m", "28/02/2015", "M", "Si", "No"},
        {"0102121019", "Cebra", "Equus Zebra", "300 Kg", "1,3 m", "15/08/1999", "F", "No", "No"},
        {"0102130308", "Churuco", "Lagothrix Lagothicha", "7 a 15 kg", "44 a 55 cm", "21/02/2011", "F", "No", "Amenazado"},
        {"0101140211", "Cigüeña Cuello Negro", "Ephippiorhynchus Asiaticus", " 4.1 kg", " 129 a 150 cm", "10/04/2009", "M", "No", "No"},
        {"0101150218", "Cisne Negro", "Cygnus Atratus", "5,1 a 8,7 kg", "1,2 a 1,4 metros", "02/10/2012", "F", "No", "No"},
        {"0101160215", "Cocli", "Theristicus Caudatus", "1,726 kg", "71 a 81 cm", "05/08/2013", "M", "No", "No"},
        {"0104170410", "Cocodrilo del Magdalena", "Crocodylus Acutus", "500 kg", "6 metros", "09/12/2010", "F", "No", "Si"},
        {"0101180205", "Condor", "Vultur Gryphus", "8 a 15 kg", "142 cm", "02/06/2011", "M", "No", "No"},
        {"0101190206", "Corocora", "Eudocimus Ruber", "0,62 kg", "66 cm", "10/07/2013", "F", "No", "No"},
        {"0105200119", "Corronchos", "Hypostomus Plecostomus", "3 a 5 kg", "30 a 40 cm", "18/03/2014", "M", "No", "No"},
        {"0101210213", "Cucaburra", "Dacelo Novaeguineae", "0,3 kg", "36 cm", "16/02/2012", "M", "No", "No"},
        {"0102221009", "Cusumbo", "Nasua", "7 kg", "60 cm ", "20/04/2010", "F", "No", "Amenazado"},
        {"0102231018", "Danta de Páramo", "Tapirus Pinchaque", "0,9 a 0,26 kg", "1,8 metros", "17/11/2007", "F", "No", "En Peligro"},
        {"0104240423", "Dragón Barbudo", "Pogona Vitticeps", "0,45 kg", "60 cm", "06/01/2008", "M", "Si", "No"},
        {"0102251002", "León", "Panthera Leo", "130 kg a 190 kg", "1.1m a 1.2m", "16/11/2005", "M", "No", "Vulnerable"},
        {"0102261002", "Tigre de Bengala", "Panthera Tigris Tigris", "100 kg a 230 kg", "0,24m a 0,31m", "20/04/2003", "F", "No", "En Peligro"},
        {"0102271009", "Hormiguero Gigante", "Myrmecophaga Tridactyla", "27kg a 41kg", "1.8m a 2.2m", "12/06/2008", "M", "No", "Vulnerable"},
        {"0102281017", "Llama", "Lama Glama", "110kg", "1m a 1.2m ", "09/04/2008", "M", "No", "Preocupacion Menor"},
        {"0102290609", "Puma", "Puma Concolor", "53kg a 100kg", "60cm a 90cm", "09/07/2006", "F", "No", "Preocupacion Menor"},
        {"0105300105", "Pirañas", "Serrasalmus", "3kg a 4kg", "15cm a 30cm", "22/03/2014", "F", "No", "Preocupacion Menor"},
        {"0105310117", "Sapoara", "Semaprochilodus Laticeps", "5kg", "44cm", "26/10/2017", "M", "No", "Preocupacion Menor"},
        {"0105320117", "Gancho Rojo", "Myloplus Rubripinnis", "2.500 g", "15cm a 30cm", "13/01/2016", "F", "No", "Preoucpacion Menor"},
        {"0105330117", "Bagre Perruno", "Perrunichthys Perruno", "5kg a 200kg", "50cm a 2m", "13/01/2016", "M", "No", "Preocupacion Menor"},
        {"0105340105", "Anguila Eléctrica", "Electrophorus Electricus", "15kg a 20kg", "1m a 2m", "08/07/2014", "M", "No", "Preoucpacion Menor"},
        {"0105350109", "Nandopsis", "Nandopsis", "1500 g", "15cm a 20cm", "15/05/2014", "F", "No", "Preocupacion Menor"},
        {"0101360208", "Loro Cachetes Amarillos", "Amazona Autumnalis", "420g", "34cm", "23/02/2013", "M", "No", "Preocupacion Menor"},
        {"0101370208", "Loro Frente Roja", "Amazona Viridigenalis", "320g", "33cm", "12/01/2014", "F", "No", "Preocupacion Menor"},
        {"0101380208", "Loro Alas de Bronce", "Pionus Chalcopterus", "210g", "30cm", "23/02/2013", "M", "No", "Preocupacion Menor"},
        {"0101391017", "Emú", "Dromaius Novaehollandiae", "30 a 45 kg", "2m", "19/02/2012", "F", "No", "Preocupacion Menor"},
        {"0101400208", "Gallina Guinea", "Numida Meleagris", "80 kg", "50 cm", "06/03/2017", "M", "No", "Preocupacion Menor"},
        {"0101410208", "Gallito Peruano", "Rupicola Peruvianus", "20 kg", "15 cm", "25/04/2015", "M", "No", "Preocupacion Menor"},
        {"0101420211", "Garceta Nevada", "Egretta Thula", "30 kg", "40 cm", "20/06/2015", "F", "No", "Preocupacion Menor"},
        {"0104430422", "Iguana", "Iguana Iguana", "20 kg", "1,50 m", "06/06/2014", "F", "No", "Preocupacion Menor"},
        {"0101440404", "Rey de los Gallinazos", "Sarcoramphus Papa", "3.4kg", "65cm a 81cm", "29/12/2010", "M", "No", "Preocupacion Menor"},
        {"0102451008", "Tapir Amazónico", "Tapirus Terrestris", "170kg", "2m", "28/01/2012", "M", "No", "Preocupacion Menor"},
        {"0104460402", "Falsa Coral", "Lampropeltis Triangulum", "1kg", "5 m", "06/08/2002", "M", "No", "No"},
        {"0104470402", "Serpiente Zumbadora", "Drymarchon Melanurus", "85 Kg", "2,1 m", "06/03/2017", "F", "No", "No"},
        {"0104480402", "Piton Bola", "Python regius", "1,5 Kg", "1,2 m", "06/03/2017", "M", "No", "No"},
        {"0104490402", "Serpiente del Maiz", "Pantherophis Guttatus", "900 g", "1,2 m", "20/07/2016", "F", "No", "No"},
        {"0104500406", "Equis", "Bothrops Asper", "1 kg", "75 cm", "08/01/2001", "M", "Si", "No"},
        {"0102511008", "Pecari de Collar", "Pecari Tajacu", "20 Kg", "90 cm", "05/11/2017", "F", "No", "No"},
        {"0102521017", "Venado de Cola Blanca", "Odocoileus Virginianus", "68 Kg", "1 m", "30/03/2007", "M", "No", "No"},
        {"0102530308", "Churucos", "Lagothrix Lagotricha", "7 Kg", "1 m", "18/04/2003", "F", "No", "Si"},
        {"0102540308", "Mono Capuchino", " Cebus Capucinus", "4 Kg", "40 cm", "14/08/2000", "F", "No", "Si"},
        {"0102550308", "Titi Ardilla", "Saimiri Sciureus", "0.5 Kg", "30 cm", "28/11/2013", "M", "No", "Si"},
        {"0102560308", "Mono Araña", "Ateles Hybridus", "2.5 Kg", "1 m", "02/07/2007", "No", "Si"},
        {"0104570402", "Anaconda", "Eunectes Murinus", "95kg", "4.5m", "19/05/2009", "M", "No", "Preocupacion Menor"},
        {"0103580709", "Rana Venenosa Verde y Negra", "Dendrobates Auratus", "0,005 Kg", " 4 cm", "21/09/2008", "F", "Si", "Si"},
        {"0103590709", "Rana Diablito", "Oophaga Sylvatica", "0,02 Kg", "3.8cm", "15/12/2017", "F", "Si", "Si"},
        {"0103600709", "Rana Kokoi", "Phyllobates Terribilis", "0,03 Kg", "7 cm", "21/09/2012", "M", "Si", "Si"},
        {"0103610709", "Candelilla Negra", "Phyllobates Aurotaenia", "0,04 Kg", "3.5cm", "12/05/2014", "F", "Si", "Si"},
        {"0103620709", "Candelilla", "Oophaga Lehmanni", "0,02 Kg", "3.5 cm", "08/11/2015", "M", "Si", "Si"},
        {"0102630317", "Tití Pigmeo", "Cebuella Pygmaea", "0,08 Kg", "12cm", "08/11/2013", "F", "No", "No"},
        {"0102641009", "Suricata", "Suricata Suricatta", "731 g", "26 cm", "17/05/2010", "F", "No", "No"},
        {"0105650109", "Mojarra Luminosa", "Andinoacara Pulcher", "0.2 Kg", "25 cm", "01/11/2017", "M", "No", "No"},
        {"0105660104", "Bocachico", "Prochilodus Magdalenae", "0.40 Kg", "65 cm", "02/10/2016", "F", "No", "No"},
        {"0105670118", "Rollizo", "Pinguipes Chilensis", "0.17 Kg", "15 cm", "31/10/2012", "M", "No", "No"},
        {"0104680809", "Tortuga Morrocoya", "Chelonoidis Carbonaria", "12 Kg", "32 cm", "23/09/2007", "F", "No", "Si"},
        {"0103690709", "Rana Venenosa Dorada", "Phyllobates Terribilis", "30 g", "62 cm", "19/06/2017", "M", "No", "Si"},
        {"0101700208", "Pava Negra", "Aburria Aburri", "1,3 Kg", "73 cm", "05/01/2010", "F", "No", "No"},
        {"0104710409", "Gecko Leopardo", "Eublepharis Macularius", "70 g", "20 cm", "15/05/2007", "M", "No", "No"},
        {"0102721002", "Hiena Manchada", "Crocuta Crocuta", "60 Kg", "1,3 m", "25/06/2011", "F", "No", "No"},
        {"0102731011", "Nutria de Río", "Lutrinae", "7.5 Kg", "65 cm", "08/08/2013", "M", "No", "Si"},
        {"0102740617", "Oso de Anteojos", "Tremarctos Ornatus", "110 Kg", " 1.8 m", "17/09/1999", "F", "No", "No"},
        {"0101750608", "Gallito de las Rocas", "Rupicola Peruvianus", "220 g", "32 cm", "08/01/2014", "M", "No", "Si"},
        {"0101761018", "Flamingo", "Phoenicopterus", "3 Kg", "1,2 m", "08/02/1997", "F", "No", "Si"},
        {"0101770211", "Pelicano", "Pelecanus", "3,4 Kg", "1,1 m", "15/08/2002", "M", "No", "No"},
        {"0101780208", "Tucan del Pacifico", "Ramphastos Brevis", "0.36 Kg", "47 cm", "18/11/2016", "F", "No", "Si"},
        {"0101790208", "Guacamaya Verde Limón", "Ara Ambiguus", "1,40 Kg", "90 cm", "05/10/1979", "M", "No", "Si"},
        {"0101800208", "Loro Cachetes Amarillos", "Amazona Autumnalis", "1.40 Kg", "90 cm", "26/02/2003", "F", "No", "No"},
        {"0105810109", "Pez Oscar", "Astronotus Ocellatus", "1.6 Kg", "35 cm", "25/05/2011", "M", "No", "No"},
        {"0201820508", "Mariposa", "Postram Heliconius", "0.0006 g", "7.3 cm", "25/08/2019", "f", "No", "No"}//
    };
    
    //codigo, habitat, intervalo de limpieza
    static String[][] habitat = {
        {"01", "Acuario", "3"},
        {"02", "Aviario", "10"},
        {"03", "Primates", "3"},
        {"04", "Reptiles", "7"},
        {"05", "Mariposario", "7"},
        {"06", "Los Andes", "7"},
        {"07", "Anfibios", "3"},
        {"08", "Tortugas", "3"},
        {"09", "Australia", "3"},
        {"10", "Otros", "3"},
    };

    //codigo, alimentacion
    static String[][] tipo_alimentacion = {
        {"01", "Animales Invertebrados", "1"},
        {"02", "Animales Vertebrados", "3"},
        {"03", "Aves", "1"},
        {"04", "Carroña", "1"},
        {"05", "Crustaceos", "1"},
        {"06", "Culebras", "1"},
        {"07", "Flores", "3"},
        {"08", "Frutos", "3"},
        {"09", "Insectos", "2"},
        {"10", "Mamiferos", "1"},
        {"11", "Peces", "3"},
        {"12", "Pequeños Mamiferos", "2"},
        {"13", "Pequeños Pajaros", "2"},
        {"14", "Pequeños Peces", "2"},
        {"15", "Pequeños Reptiles", "2"},
        {"16", "Pequeños Roedores", "2"},
        {"17", "Plantas", "5"},
        {"18", "Plantas Acuaticas", "4"},
        {"19", "Plantas Secas", "4"},
        {"20", "Ranas", "3"},
        {"21", "Reptiles", "2"},
        {"22", "Vegetales", "5"}
    };

    //Codigo, cargo
    static String[][] cargos = {
        {"01", "Cirujia"},
        {"02", "Oncologia"},
        {"03", "Fisioterapia"},
        {"04", "Rehabilitación"},
        {"05", "Imagenología"},
        {"06", "Fauna Silvestre"},
        {"07", "Cardiología"},
        {"08", "Oftalmología"},
        {"09", "Oncología"},
        {"10", "Anestesiología"},
        {"11", "Limpieza"},
        {"12", "Cuidador"},
        {"13", "Seguridad"},
        {"14", "Administrador Sistema"}
    };

    //codigo: **id_veterinario, **cargo , **usuario
    //codigo, nombre, apellido, fecha_nacimiento, sexo, telefono, direccion
    static String[][] veterinario = {
        {"010108", "Juan Carlos", "Guzamn", "17/04/1987", "m", "6666666", "Cl4 #56-54"},
        {"020207", "Maria Camila", "Restrepo", "17/04/1987", "m", "6666666", "Cl4 #56-54"},
        {"030106", "Daniela", "Ospina", "17/04/1987", "m", "6666666", "Cl4 #56-54"}
    };
    
    //codigo: **id_empleado, **cargo, **usuario
    //codigo, nombre, apellido, fecha_nacimiento, sexo, telefono, direccion
    static String[][] empleados = {
        {"011105", "Pedro", "Ramirez", "17/04/1987", "m", "6666666", "Cl4 #56-54"},
        {"021204", "Manuela", "Gaizman", "17/04/1987", "m", "6666666", "Cl4 #56-54"},
        {"031303", "Juan", "Baptiste", "17/04/1987", "m", "6666666", "Cl4 #56-54"},
        {"041202", "Maria",  "del mar", "17/04/1987", "m", "6666666", "Cl4 #56-54"},
        {"051401", "Jose", "Ramirez", "17/04/1987", "m", "6666666", "Cl4 #56-54"}
    };
    
    //Codigo, usuario, clave
    static String[][] Usuarios = {
        {"01", "jramirez", "12345"},
        {"02", "mdmar", "12345"},
        {"03", "jbaptiste", "12345"},
        {"04", "mgaizman", "12345"},
        {"05", "pramirez", "12345"},
        {"06", "dospina", "12345"},
        {"07", "mcrestrepo", "12345"},
        {"08", "jcguzamm", "12345"}
    };

    //Codigo, servicio, Desc
    static String[][] servicios = {
        {"01", "Hospitalizacion", "Atención y medicina veterinaria para los animales y mascotas."},
        {"02", "Medicina Interna", "Diagnóstico y tratamiento de enfermedades animales."},
        {"03", "Odontologia", "Cuidamos la dentadura de todo tipo de animales y mascotas."},
        {"04", "Traumatología", "Traumatología animal. Fracturas. Radiografías, escayolas..."},
        {"05", "Cirugía", "Realizamos todo tipo de operaciones quirúrgicas en animales."},
        {"06", "Reproducción", "Embarazos de animales. partos, obstetricia, neonatología..."},
        {"07", "Ecografías", "Realizamos ecografías de animales para diagnóstico de enfermedades."},
        {"08", "Endoscopias", "Endoscopias en animales para diagnóstico y curación de enfermedades."}
    };

    //Codigo, estado
    static String[][] estado = {
        {"01", "Saludable"},
        {"02", "Pérdida del apetito"},
        {"03", "Consumo excesivo de agua"},
        {"04", "Aumento o pérdida de peso en forma rápida"},
        {"05", "Comportamiento fuera de lo común"},
        {"06", "Cansancio y pereza"},
        {"07", "Dificultad para levantarse o acostarse"},
        {"08", "Abultamientos extraños"}
    };
}
