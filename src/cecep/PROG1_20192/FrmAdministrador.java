package cecep.PROG1_20192;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jose R
 */
public class FrmAdministrador extends javax.swing.JFrame {
    public static InfoSesion sesion;

    public FrmAdministrador(InfoSesion sesion) {
        initComponents();
        this.sesion = sesion;
        this.setLocationRelativeTo(null);//establece la posicion de la ventana en el centro
        ImageIcon FondMap = new ImageIcon("src/Imagenes/FondoFrmAdmin.png");//define la ruta de la imagen y le asignamos una escala a la imagen
        ImageIcon FondMap1 = new ImageIcon(FondMap.getImage().getScaledInstance(LB_ImgFondo.getWidth(), LB_ImgFondo.getHeight(), Image.SCALE_DEFAULT));
        LB_ImgFondo.setIcon(FondMap1);//fondo de la imagen

        llenarBitacora();//Se llama el metodo llenar bitacora
        //se llama la funcion que crea y asigna el modelo de cada tabla
        ModeloTabla(Matrices.animales, Table_Animales);
        ModeloTabla(Matrices.Usuarios, Table_Usuarios);
        ModeloTabla(Matrices.empleados, Table_Empleados);
        ModeloTabla(Matrices.veterinario, Table_Veterinarios);

    }

    private void ModeloTabla(String[][] datos, JTable table){
        //Acepta como paramettro uma matriz de cadena y una tabla 
        int col_count = table.getColumnCount();//reside la cantidad de columnas de la tabla
        String[] columnas = new String[col_count];//crea un vector de cadenas de columnas con una tamaño de la cantidad que se
        //recibe arriba
        for (int i = 0; i < col_count; i++) {//recorre la tabla
            columnas[i]=table.getColumnName(i);//guarda el nombre de la columna en el vector
        }
        DefaultTableModel dtm = new DefaultTableModel(datos, columnas);//uso como parametros la matriz pasada al metodo
        //y las columnas en el vector creado arriba
        dtm.addTableModelListener((tme) -> {//agrego un listener con una expresion lambda
            int col = table.getSelectedColumn();//guarda la columna seleccionada
            int row = table.getSelectedRow();//guarda la fila seleccionada
            datos[row][col]=(String)(table.getValueAt(row, col));//guarda los cambios hechos en la tabla en la matriz
        });
        table.setModel(dtm);//agrega el modelo de la tabla
    }
    private void llenarBitacora(){
        String msg="";//inicia la variable
        for (Object log_element : sesion.getLog()){
            msg+=(String)log_element;
        }
        TA_Bitacora.setText(msg);//la variable se le asigna el mensaje de el para extendido
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btn_cerrar = new javax.swing.JButton();
        LB_ImgFondo = new javax.swing.JLabel();
        TabbedPane = new javax.swing.JTabbedPane();
        Spane_Bitacora = new javax.swing.JScrollPane();
        TA_Bitacora = new javax.swing.JTextArea();
        Spane_Animales = new javax.swing.JScrollPane();
        Table_Animales = new javax.swing.JTable();
        Spane_Usuarios = new javax.swing.JScrollPane();
        Table_Usuarios = new javax.swing.JTable();
        Spane_Empleados = new javax.swing.JScrollPane();
        Table_Empleados = new javax.swing.JTable();
        Spane_Veterinarios = new javax.swing.JScrollPane();
        Table_Veterinarios = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_cerrar.setBackground(new java.awt.Color(255, 255, 255));
        btn_cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/CerrarIcono.png"))); // NOI18N
        btn_cerrar.setFocusable(false);
        btn_cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_cerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 0, 20, 20));
        getContentPane().add(LB_ImgFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 780, 450));

        TabbedPane.setBackground(new java.awt.Color(204, 255, 204));
        TabbedPane.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        TabbedPane.setMaximumSize(new java.awt.Dimension(100, 100));
        TabbedPane.setMinimumSize(new java.awt.Dimension(100, 100));
        TabbedPane.setOpaque(true);

        TA_Bitacora.setEditable(false);
        TA_Bitacora.setColumns(20);
        TA_Bitacora.setRows(5);
        Spane_Bitacora.setViewportView(TA_Bitacora);

        TabbedPane.addTab("Bitacora", Spane_Bitacora);

        Table_Animales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nombre", "N.Cientifico", "Peso", "Estatura", "Fec. Nac", "Sexo", "Venenoso", "P.extincion"
            }
        ));
        Table_Animales.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        Table_Animales.setShowGrid(true);
        Table_Animales.getTableHeader().setReorderingAllowed(false);
        Spane_Animales.setViewportView(Table_Animales);

        TabbedPane.addTab("Animales", Spane_Animales);

        Table_Usuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Codigo", "Usuario", "Clave"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        Table_Usuarios.setColumnSelectionAllowed(true);
        Table_Usuarios.getTableHeader().setReorderingAllowed(false);
        Spane_Usuarios.setViewportView(Table_Usuarios);
        Table_Usuarios.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (Table_Usuarios.getColumnModel().getColumnCount() > 0) {
            Table_Usuarios.getColumnModel().getColumn(0).setResizable(false);
            Table_Usuarios.getColumnModel().getColumn(1).setResizable(false);
            Table_Usuarios.getColumnModel().getColumn(2).setResizable(false);
        }

        TabbedPane.addTab("Usuarios", Spane_Usuarios);

        Table_Empleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nombre", "Apellido", "F. Nacimiento", "Sexo", "Telefono", "Direccion"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        Table_Empleados.setColumnSelectionAllowed(true);
        Table_Empleados.getTableHeader().setReorderingAllowed(false);
        Spane_Empleados.setViewportView(Table_Empleados);
        Table_Empleados.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (Table_Empleados.getColumnModel().getColumnCount() > 0) {
            Table_Empleados.getColumnModel().getColumn(0).setResizable(false);
            Table_Empleados.getColumnModel().getColumn(1).setResizable(false);
            Table_Empleados.getColumnModel().getColumn(2).setResizable(false);
            Table_Empleados.getColumnModel().getColumn(3).setResizable(false);
            Table_Empleados.getColumnModel().getColumn(4).setResizable(false);
        }

        TabbedPane.addTab("Empleados", Spane_Empleados);

        Table_Veterinarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nombre", "Apellido", "F. Nacimiento", "Sexo", "Telefono", "Direccion"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        Table_Veterinarios.setColumnSelectionAllowed(true);
        Table_Veterinarios.getTableHeader().setReorderingAllowed(false);
        Spane_Veterinarios.setViewportView(Table_Veterinarios);
        Table_Veterinarios.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (Table_Veterinarios.getColumnModel().getColumnCount() > 0) {
            Table_Veterinarios.getColumnModel().getColumn(0).setResizable(false);
            Table_Veterinarios.getColumnModel().getColumn(1).setResizable(false);
            Table_Veterinarios.getColumnModel().getColumn(2).setResizable(false);
            Table_Veterinarios.getColumnModel().getColumn(3).setResizable(false);
            Table_Veterinarios.getColumnModel().getColumn(5).setResizable(false);
            Table_Veterinarios.getColumnModel().getColumn(6).setResizable(false);
        }

        TabbedPane.addTab("Veterinarios", Spane_Veterinarios);

        getContentPane().add(TabbedPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 600, 390));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cerrarActionPerformed
        // TODO add your handling code here:
        //declaramos la variable de confirmar
        int confirmar = JOptionPane.showConfirmDialog(rootPane, "Estas seguro que deseas volver al inicio?");// ventana de dialogo para confirmar si el usuario desea regresar
        if (JOptionPane.OK_OPTION == confirmar) {// condicional que me permite evaluar la respouesta del usuario
            FrmLogin a = new FrmLogin(sesion);//Se cra un objeto de tipo FRM login con los parametros de la clase Infosesion alojada en la variable sesion
            a.setVisible(true);//Se muestra el objeto creado anteriormente 
            dispose();//Se cierra la ventana actual
        } else if (JOptionPane.CANCEL_OPTION == confirmar) {
            return;// se devualve a la ventana actual sin realizar ninguna acción
        }
    }//GEN-LAST:event_btn_cerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmAdministrador(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LB_ImgFondo;
    private javax.swing.JScrollPane Spane_Animales;
    private javax.swing.JScrollPane Spane_Bitacora;
    private javax.swing.JScrollPane Spane_Empleados;
    private javax.swing.JScrollPane Spane_Usuarios;
    private javax.swing.JScrollPane Spane_Veterinarios;
    private javax.swing.JTextArea TA_Bitacora;
    private javax.swing.JTabbedPane TabbedPane;
    private javax.swing.JTable Table_Animales;
    private javax.swing.JTable Table_Empleados;
    private javax.swing.JTable Table_Usuarios;
    private javax.swing.JTable Table_Veterinarios;
    private javax.swing.JButton btn_cerrar;
    // End of variables declaration//GEN-END:variables
}
